import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import {MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule} from '@angular/material';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { MonitoringComponent } from './monitoring/monitoring.component';
import { AgentComponent } from './agent/agent.component';
import { FinanceComponent } from './finance/finance.component';
import { PostComponent } from './post/post.component';
import { QueueComponent } from './queue/queue.component';
import { OnlineComponent } from './online/online.component';
import { GeneralStatComponent } from './general-stat/general-stat.component';
import { FilpasportComponent } from './filpasport/filpasport.component';
import { TrakingComponent } from './traking/traking.component';
import { MonitoringItComponent } from './monitoring-it/monitoring-it.component';
import { IspComponent } from './isp/isp.component';
import { OtchetySeoComponent } from './otchety-seo/otchety-seo.component';


@NgModule({
  declarations: [
    AppComponent,
    MonitoringComponent,
    AgentComponent,
    FinanceComponent,
    PostComponent,
    QueueComponent,
    OnlineComponent,
    GeneralStatComponent,
    FilpasportComponent,
    TrakingComponent,
    MonitoringItComponent,
    IspComponent,
    OtchetySeoComponent
  ],
  imports: [ 
    BrowserModule,  BrowserAnimationsModule,
    MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule,
  AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
