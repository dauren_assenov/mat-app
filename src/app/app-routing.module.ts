import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AgentComponent } from './agent/agent.component';
import { FinanceComponent } from './finance/finance.component';
import { PostComponent } from './post/post.component';
import { MonitoringComponent } from './monitoring/monitoring.component';

const routes: Routes = [
  { path: '', redirectTo: '/finance', pathMatch: 'full' },
  { path: 'agent', component: AgentComponent },
  { path: 'monitoring', component: MonitoringComponent },
  { path: 'finance', component: FinanceComponent },
  { path: 'post', component: PostComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
